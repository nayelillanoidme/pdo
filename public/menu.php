<?php
// Obtener la lista de procedencias disponibles en la tabla de usuarios
$procedencias = obtenerProcedencias(); // Reemplaza 'obtenerProcedencias()' con la función que obtiene la lista de procedencias desde la base de datos

// Verificar si se ha enviado un filtro y almacenarlo en una variable
$filtro = isset($_GET['filtro']) ? $_GET['filtro'] : '';

// Imprimir el menú de filtro
echo '<select name="filtro_procedencia">';
echo '<option value="">Mostrar todos</option>'; // opción para mostrar todos los usuarios sin filtrar

foreach ($procedencias as $procedencia) {
    // Seleccionar la opción si coincide con el filtro actual
    $selected = ($filtro == $procedencia) ? 'selected' : '';
    echo '<option value="' . $procedencia . '" ' . $selected . '>' . $procedencia . '</option>';
}

echo '</select>';
?>

<!-- Agrega el siguiente código JavaScript para enviar el formulario cuando se selecciona una opción -->
<script>
    document.querySelector('select[name="filtro_procedencia"]').addEventListener('change', function() {
        var filtro = this.value;
        window.location.href = 'leer.php?filtro=' + filtro;
    });
</script>
